-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: demo
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `eo_crsd_languages`
--

DROP TABLE IF EXISTS `eo_crsd_languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eo_crsd_languages` (
  `Language_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Language_in_English` varchar(200) NOT NULL,
  `Language_in_Local` varchar(300) NOT NULL,
  PRIMARY KEY (`Language_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eo_crsd_languages`
--

LOCK TABLES `eo_crsd_languages` WRITE;
/*!40000 ALTER TABLE `eo_crsd_languages` DISABLE KEYS */;
INSERT INTO `eo_crsd_languages` VALUES (1,'Tamil','தமிழர்கள்'),(9,'English','English'),(11,'Arabic','العَرَبِيَّة‎'),(12,'','العَرَبِيَّة‎'),(13,'','العَرَبِيَّة‎'),(14,'','العَرَبِيَّة‎'),(15,'','العَرَبِيَّة‎'),(16,'Arabic1','العَرَبِيَّة‎'),(17,'Arabic112','العَرَبِيَّة‎'),(18,'Arabic1123','العَرَبِيَّة‎'),(19,'Arabic1123','العَرَبِيَّة‎'),(20,'Arabic1123','العَرَبِيَّة‎'),(21,'Arabic1123','العَرَبِيَّة‎');
/*!40000 ALTER TABLE `eo_crsd_languages` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-31 13:02:33
