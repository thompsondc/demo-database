-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: emmaus
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `eo_cntd_contacts`
--

DROP TABLE IF EXISTS `eo_cntd_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eo_cntd_contacts` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(45) DEFAULT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `ct_address1` varchar(200) DEFAULT NULL,
  `ct_address2` varchar(100) DEFAULT NULL,
  `ct_address3` varchar(100) DEFAULT NULL,
  `ct_address4` varchar(100) DEFAULT NULL,
  `ct_pincode` int(11) DEFAULT NULL,
  `ct_mobile` varchar(20) DEFAULT NULL,
  `ct_gender` varchar(7) DEFAULT NULL,
  `ct_mail_id` varchar(45) NOT NULL,
  `ct_photo` blob,
  `ct_assembly_reference` varchar(200) DEFAULT NULL,
  `ct_parent_id` int(11) DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `ct_active` varchar(10) DEFAULT NULL,
  `Image_extn` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`contact_id`),
  KEY `contacts_user_fk_id_idx` (`user_id`),
  CONSTRAINT `contacts_user_fk_id` FOREIGN KEY (`user_id`) REFERENCES `eo_users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eo_cntd_contacts`
--

LOCK TABLES `eo_cntd_contacts` WRITE;
/*!40000 ALTER TABLE `eo_cntd_contacts` DISABLE KEYS */;
INSERT INTO `eo_cntd_contacts` VALUES (1,'ADC3','Admin','-','address','Main Road','sadf','Nellore',43543,'9441234567','Male','paulbca@gmail.com',NULL,'Bethany Assembly',0,1,'Active','.jpg');
/*!40000 ALTER TABLE `eo_cntd_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eo_crsd_courses`
--

DROP TABLE IF EXISTS `eo_crsd_courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eo_crsd_courses` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_title` varchar(200) NOT NULL,
  `course_code` varchar(45) DEFAULT NULL,
  `c_language_id` int(11) NOT NULL,
  `c_english_script` varchar(45) DEFAULT NULL,
  `c_local_script` varchar(45) DEFAULT NULL,
  `display_order` int(11) DEFAULT '0',
  PRIMARY KEY (`course_id`),
  KEY `courses_lan_fk_id_idx` (`c_language_id`),
  CONSTRAINT `courses_lan_fk_id` FOREIGN KEY (`c_language_id`) REFERENCES `eo_crsd_languages` (`Language_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eo_crsd_courses`
--

LOCK TABLES `eo_crsd_courses` WRITE;
/*!40000 ALTER TABLE `eo_crsd_courses` DISABLE KEYS */;
/*!40000 ALTER TABLE `eo_crsd_courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eo_crsd_languages`
--

DROP TABLE IF EXISTS `eo_crsd_languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eo_crsd_languages` (
  `Language_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Language_in_English` varchar(200) NOT NULL,
  `Language_in_Local` varchar(300) NOT NULL,
  PRIMARY KEY (`Language_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Course Details - Languages';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eo_crsd_languages`
--

LOCK TABLES `eo_crsd_languages` WRITE;
/*!40000 ALTER TABLE `eo_crsd_languages` DISABLE KEYS */;
/*!40000 ALTER TABLE `eo_crsd_languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eo_crsd_my_stock`
--

DROP TABLE IF EXISTS `eo_crsd_my_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eo_crsd_my_stock` (
  `stock_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_instock` int(11) DEFAULT NULL,
  `c_distributed` int(11) DEFAULT NULL,
  `c_returned` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `c_date` date DEFAULT NULL,
  `c_status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`stock_id`),
  KEY `course_id_fk_id_idx` (`course_id`),
  KEY `mystock_user_fk_id_idx` (`user_id`),
  CONSTRAINT `course_id_fk_id` FOREIGN KEY (`course_id`) REFERENCES `eo_crsd_courses` (`course_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `mystock_user_fk_id` FOREIGN KEY (`user_id`) REFERENCES `eo_users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eo_crsd_my_stock`
--

LOCK TABLES `eo_crsd_my_stock` WRITE;
/*!40000 ALTER TABLE `eo_crsd_my_stock` DISABLE KEYS */;
/*!40000 ALTER TABLE `eo_crsd_my_stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eo_email_config`
--

DROP TABLE IF EXISTS `eo_email_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eo_email_config` (
  `email_config_id` int(11) NOT NULL AUTO_INCREMENT,
  `host_name` varchar(45) NOT NULL,
  `email_id` varchar(45) NOT NULL,
  `email_password` varchar(45) NOT NULL,
  `is_active` varchar(3) NOT NULL,
  PRIMARY KEY (`email_config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eo_email_config`
--

LOCK TABLES `eo_email_config` WRITE;
/*!40000 ALTER TABLE `eo_email_config` DISABLE KEYS */;
INSERT INTO `eo_email_config` VALUES (1,'smtp.gmail.com','infoemmausindia@gmail.com','Bangalore!234','Yes');
/*!40000 ALTER TABLE `eo_email_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eo_email_sent_details`
--

DROP TABLE IF EXISTS `eo_email_sent_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eo_email_sent_details` (
  `email_sent_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_body` varchar(500) DEFAULT NULL,
  `email_subject` varchar(100) DEFAULT NULL,
  `send_by` int(11) DEFAULT NULL,
  `send_to` varchar(45) DEFAULT NULL,
  `email_status` varchar(15) DEFAULT NULL,
  `send_date` datetime DEFAULT NULL,
  PRIMARY KEY (`email_sent_details_id`),
  KEY `sent_by_fk_id_idx` (`send_by`),
  CONSTRAINT `email_sent_by_fk_id` FOREIGN KEY (`send_by`) REFERENCES `eo_users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eo_email_sent_details`
--

LOCK TABLES `eo_email_sent_details` WRITE;
/*!40000 ALTER TABLE `eo_email_sent_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `eo_email_sent_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eo_languages_assigned`
--

DROP TABLE IF EXISTS `eo_languages_assigned`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eo_languages_assigned` (
  `languages_assigned_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`languages_assigned_id`),
  KEY `lanassign_fk_id_idx` (`language_id`),
  KEY `lanassign_user_fk_id_idx` (`user_id`),
  CONSTRAINT `lanassign_fk_id` FOREIGN KEY (`language_id`) REFERENCES `eo_crsd_languages` (`Language_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `lanassign_user_fk_id` FOREIGN KEY (`user_id`) REFERENCES `eo_users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eo_languages_assigned`
--

LOCK TABLES `eo_languages_assigned` WRITE;
/*!40000 ALTER TABLE `eo_languages_assigned` DISABLE KEYS */;
/*!40000 ALTER TABLE `eo_languages_assigned` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eo_login`
--

DROP TABLE IF EXISTS `eo_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eo_login` (
  `login_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `user_password` varchar(50) NOT NULL,
  PRIMARY KEY (`login_id`),
  KEY `login_user_fk_id_idx` (`user_id`),
  CONSTRAINT `login_user_fk_id` FOREIGN KEY (`user_id`) REFERENCES `eo_users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eo_login`
--

LOCK TABLES `eo_login` WRITE;
/*!40000 ALTER TABLE `eo_login` DISABLE KEYS */;
INSERT INTO `eo_login` VALUES (1,1,'admin@ecsweb.in','rYr52W0TW04=');
/*!40000 ALTER TABLE `eo_login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eo_login_logs`
--

DROP TABLE IF EXISTS `eo_login_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eo_login_logs` (
  `login_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `login_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `logout_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`login_log_id`),
  KEY `login_log_user_id_idx` (`user_id`),
  CONSTRAINT `login_log_user_id` FOREIGN KEY (`user_id`) REFERENCES `eo_users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='login and logout details for all users; ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eo_login_logs`
--

LOCK TABLES `eo_login_logs` WRITE;
/*!40000 ALTER TABLE `eo_login_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `eo_login_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eo_pincode`
--

DROP TABLE IF EXISTS `eo_pincode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eo_pincode` (
  `pincode_id` int(11) NOT NULL AUTO_INCREMENT,
  `pincode` int(11) NOT NULL,
  `officename` varchar(45) NOT NULL,
  `divisionname` varchar(45) DEFAULT NULL,
  `regionname` varchar(45) DEFAULT NULL,
  `circlename` varchar(45) DEFAULT NULL,
  `Taluk` varchar(45) NOT NULL,
  `Districtname` varchar(45) NOT NULL,
  `statename` varchar(45) NOT NULL,
  PRIMARY KEY (`pincode_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='All india pincode';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eo_pincode`
--

LOCK TABLES `eo_pincode` WRITE;
/*!40000 ALTER TABLE `eo_pincode` DISABLE KEYS */;
/*!40000 ALTER TABLE `eo_pincode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eo_stud_courses`
--

DROP TABLE IF EXISTS `eo_stud_courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eo_stud_courses` (
  `stud_course_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `is_distributed` varchar(3) DEFAULT 'No',
  `distributed_date` date DEFAULT NULL,
  `is_returned` varchar(3) DEFAULT 'No',
  `returned_date` date DEFAULT NULL,
  `is_corrected` varchar(3) DEFAULT 'No',
  `corrected_date` date DEFAULT NULL,
  `sc_marks` int(11) DEFAULT '0',
  `student_comments` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`stud_course_id`),
  KEY `studcors_student_fk_id_idx` (`student_id`),
  KEY `studcors_course_fk_id_idx` (`course_id`),
  KEY `studcors_language_fk_id_idx` (`language_id`),
  CONSTRAINT `studcors_course_fk_id` FOREIGN KEY (`course_id`) REFERENCES `eo_crsd_courses` (`course_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `studcors_language_fk_id` FOREIGN KEY (`language_id`) REFERENCES `eo_crsd_languages` (`Language_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `studcors_student_fk_id` FOREIGN KEY (`student_id`) REFERENCES `eo_stud_students` (`student_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eo_stud_courses`
--

LOCK TABLES `eo_stud_courses` WRITE;
/*!40000 ALTER TABLE `eo_stud_courses` DISABLE KEYS */;
/*!40000 ALTER TABLE `eo_stud_courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eo_stud_students`
--

DROP TABLE IF EXISTS `eo_stud_students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eo_stud_students` (
  `student_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_code` varchar(45) DEFAULT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `st_address1` varchar(200) DEFAULT NULL,
  `st_address2` varchar(100) DEFAULT NULL,
  `st_address3` varchar(100) DEFAULT NULL,
  `st_address4` varchar(100) DEFAULT NULL,
  `st_pincode` int(11) DEFAULT NULL,
  `st_mobile` varchar(20) DEFAULT NULL,
  `st_gender` varchar(7) DEFAULT NULL,
  `st_mail_id` varchar(45) NOT NULL,
  `st_photo` blob,
  `user_id` int(11) NOT NULL,
  `st_active` varchar(10) DEFAULT NULL,
  `creator_id` int(11) NOT NULL,
  `st_comments` varchar(500) DEFAULT NULL,
  `Image_extn` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`student_id`),
  KEY `students_user_fk_id_id_idx` (`user_id`),
  KEY `students_creator_fk_id_idx` (`creator_id`),
  CONSTRAINT `students_creator_fk_id` FOREIGN KEY (`creator_id`) REFERENCES `eo_users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `students_user_fk_id_id` FOREIGN KEY (`user_id`) REFERENCES `eo_users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eo_stud_students`
--

LOCK TABLES `eo_stud_students` WRITE;
/*!40000 ALTER TABLE `eo_stud_students` DISABLE KEYS */;
/*!40000 ALTER TABLE `eo_stud_students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eo_user_types`
--

DROP TABLE IF EXISTS `eo_user_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eo_user_types` (
  `user_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_description` varchar(100) NOT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eo_user_types`
--

LOCK TABLES `eo_user_types` WRITE;
/*!40000 ALTER TABLE `eo_user_types` DISABLE KEYS */;
INSERT INTO `eo_user_types` VALUES (1,'Student'),(2,'Coordinator'),(3,'Administrator'),(4,'Distributor'),(5,'Sub-Coordinator');
/*!40000 ALTER TABLE `eo_user_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eo_users`
--

DROP TABLE IF EXISTS `eo_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eo_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `user_type_fk_id_idx` (`user_type_id`),
  CONSTRAINT `users_user_type_fk_id` FOREIGN KEY (`user_type_id`) REFERENCES `eo_user_types` (`user_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eo_users`
--

LOCK TABLES `eo_users` WRITE;
/*!40000 ALTER TABLE `eo_users` DISABLE KEYS */;
INSERT INTO `eo_users` VALUES (1,3);
/*!40000 ALTER TABLE `eo_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `v_assigned_course_students_languages`
--

DROP TABLE IF EXISTS `v_assigned_course_students_languages`;
/*!50001 DROP VIEW IF EXISTS `v_assigned_course_students_languages`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_assigned_course_students_languages` AS SELECT 
 1 AS `stud_course_id`,
 1 AS `student_id`,
 1 AS `student_code`,
 1 AS `first_name`,
 1 AS `last_name`,
 1 AS `course_id`,
 1 AS `language_id`,
 1 AS `is_distributed`,
 1 AS `distributed_date`,
 1 AS `is_returned`,
 1 AS `returned_date`,
 1 AS `is_corrected`,
 1 AS `corrected_date`,
 1 AS `sc_marks`,
 1 AS `student_comments`,
 1 AS `Language_in_English`,
 1 AS `Language_in_Local`,
 1 AS `course_title`,
 1 AS `course_code`,
 1 AS `c_english_script`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_contacts_login_users_usertypes`
--

DROP TABLE IF EXISTS `v_contacts_login_users_usertypes`;
/*!50001 DROP VIEW IF EXISTS `v_contacts_login_users_usertypes`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_contacts_login_users_usertypes` AS SELECT 
 1 AS `contact_id`,
 1 AS `user_code`,
 1 AS `first_name`,
 1 AS `last_name`,
 1 AS `ct_address1`,
 1 AS `ct_address2`,
 1 AS `ct_address3`,
 1 AS `ct_address4`,
 1 AS `ct_pincode`,
 1 AS `ct_mobile`,
 1 AS `ct_gender`,
 1 AS `ct_mail_id`,
 1 AS `ct_photo`,
 1 AS `ct_assembly_reference`,
 1 AS `user_id`,
 1 AS `ct_active`,
 1 AS `login_id`,
 1 AS `user_name`,
 1 AS `user_password`,
 1 AS `user_type_id`,
 1 AS `user_type_description`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_courses_languages`
--

DROP TABLE IF EXISTS `v_courses_languages`;
/*!50001 DROP VIEW IF EXISTS `v_courses_languages`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_courses_languages` AS SELECT 
 1 AS `course_id`,
 1 AS `course_code`,
 1 AS `course_title`,
 1 AS `c_language_id`,
 1 AS `c_english_script`,
 1 AS `c_local_script`,
 1 AS `Language_in_English`,
 1 AS `Language_in_Local`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_crsd_my_stock_contacts_courses`
--

DROP TABLE IF EXISTS `v_crsd_my_stock_contacts_courses`;
/*!50001 DROP VIEW IF EXISTS `v_crsd_my_stock_contacts_courses`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_crsd_my_stock_contacts_courses` AS SELECT 
 1 AS `stock_id`,
 1 AS `c_date`,
 1 AS `c_instock`,
 1 AS `c_distributed`,
 1 AS `c_returned`,
 1 AS `user_id`,
 1 AS `course_id`,
 1 AS `receiver_id`,
 1 AS `c_status`,
 1 AS `first_name`,
 1 AS `user_code`,
 1 AS `course_title`,
 1 AS `c_language_id`,
 1 AS `course_code`,
 1 AS `Language_in_English`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_languages_contacts`
--

DROP TABLE IF EXISTS `v_languages_contacts`;
/*!50001 DROP VIEW IF EXISTS `v_languages_contacts`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_languages_contacts` AS SELECT 
 1 AS `user_id`,
 1 AS `contact_id`,
 1 AS `user_code`,
 1 AS `first_name`,
 1 AS `last_name`,
 1 AS `ct_address1`,
 1 AS `ct_address2`,
 1 AS `ct_address3`,
 1 AS `ct_address4`,
 1 AS `ct_pincode`,
 1 AS `ct_mobile`,
 1 AS `ct_gender`,
 1 AS `ct_mail_id`,
 1 AS `ct_photo`,
 1 AS `ct_assembly_reference`,
 1 AS `ct_active`,
 1 AS `user_type_id`,
 1 AS `user_type_description`,
 1 AS `languages_assigned_id`,
 1 AS `language_id`,
 1 AS `Language_in_English`,
 1 AS `Language_in_Local`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_languages_students`
--

DROP TABLE IF EXISTS `v_languages_students`;
/*!50001 DROP VIEW IF EXISTS `v_languages_students`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_languages_students` AS SELECT 
 1 AS `user_id`,
 1 AS `student_id`,
 1 AS `student_code`,
 1 AS `first_name`,
 1 AS `last_name`,
 1 AS `st_address1`,
 1 AS `st_address2`,
 1 AS `st_address3`,
 1 AS `st_address4`,
 1 AS `st_pincode`,
 1 AS `st_mobile`,
 1 AS `st_gender`,
 1 AS `st_mail_id`,
 1 AS `st_photo`,
 1 AS `st_active`,
 1 AS `user_type_id`,
 1 AS `user_type_description`,
 1 AS `languages_assigned_id`,
 1 AS `language_id`,
 1 AS `Language_in_English`,
 1 AS `Language_in_Local`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_languages_users`
--

DROP TABLE IF EXISTS `v_languages_users`;
/*!50001 DROP VIEW IF EXISTS `v_languages_users`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_languages_users` AS SELECT 
 1 AS `user_id`,
 1 AS `languages_assigned_id`,
 1 AS `language_id`,
 1 AS `Language_in_English`,
 1 AS `Language_in_Local`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `v_assigned_course_students_languages`
--

/*!50001 DROP VIEW IF EXISTS `v_assigned_course_students_languages`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_assigned_course_students_languages` AS select `b`.`stud_course_id` AS `stud_course_id`,`a`.`student_id` AS `student_id`,`a`.`student_code` AS `student_code`,`a`.`first_name` AS `first_name`,`a`.`last_name` AS `last_name`,`b`.`course_id` AS `course_id`,`b`.`language_id` AS `language_id`,`b`.`is_distributed` AS `is_distributed`,`b`.`distributed_date` AS `distributed_date`,`b`.`is_returned` AS `is_returned`,`b`.`returned_date` AS `returned_date`,`b`.`is_corrected` AS `is_corrected`,`b`.`corrected_date` AS `corrected_date`,`b`.`sc_marks` AS `sc_marks`,`b`.`student_comments` AS `student_comments`,`c`.`Language_in_English` AS `Language_in_English`,`c`.`Language_in_Local` AS `Language_in_Local`,`d`.`course_title` AS `course_title`,`d`.`course_code` AS `course_code`,`d`.`c_english_script` AS `c_english_script` from (((`eo_stud_students` `a` join `eo_stud_courses` `b`) join `eo_crsd_languages` `c`) join `eo_crsd_courses` `d`) where ((`a`.`student_id` = `b`.`student_id`) and (`b`.`language_id` = `c`.`Language_ID`) and (`b`.`course_id` = `d`.`course_id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_contacts_login_users_usertypes`
--

/*!50001 DROP VIEW IF EXISTS `v_contacts_login_users_usertypes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_contacts_login_users_usertypes` AS (select `a`.`contact_id` AS `contact_id`,`a`.`user_code` AS `user_code`,`a`.`first_name` AS `first_name`,`a`.`last_name` AS `last_name`,`a`.`ct_address1` AS `ct_address1`,`a`.`ct_address2` AS `ct_address2`,`a`.`ct_address3` AS `ct_address3`,`a`.`ct_address4` AS `ct_address4`,`a`.`ct_pincode` AS `ct_pincode`,`a`.`ct_mobile` AS `ct_mobile`,`a`.`ct_gender` AS `ct_gender`,`a`.`ct_mail_id` AS `ct_mail_id`,`a`.`ct_photo` AS `ct_photo`,`a`.`ct_assembly_reference` AS `ct_assembly_reference`,`a`.`user_id` AS `user_id`,`a`.`ct_active` AS `ct_active`,`b`.`login_id` AS `login_id`,`b`.`user_name` AS `user_name`,`b`.`user_password` AS `user_password`,`c`.`user_type_id` AS `user_type_id`,`d`.`user_type_description` AS `user_type_description` from (((`eo_cntd_contacts` `a` join `eo_login` `b`) join `eo_users` `c`) join `eo_user_types` `d`) where ((`a`.`user_id` = `b`.`user_id`) and (`a`.`user_id` = `c`.`user_id`) and (`c`.`user_type_id` = `d`.`user_type_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_courses_languages`
--

/*!50001 DROP VIEW IF EXISTS `v_courses_languages`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_courses_languages` AS (select distinct `a`.`course_id` AS `course_id`,`a`.`course_code` AS `course_code`,`a`.`course_title` AS `course_title`,`a`.`c_language_id` AS `c_language_id`,`a`.`c_english_script` AS `c_english_script`,`a`.`c_local_script` AS `c_local_script`,`b`.`Language_in_English` AS `Language_in_English`,`b`.`Language_in_Local` AS `Language_in_Local` from (`eo_crsd_courses` `a` join `eo_crsd_languages` `b`) where (`a`.`c_language_id` = `b`.`Language_ID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_crsd_my_stock_contacts_courses`
--

/*!50001 DROP VIEW IF EXISTS `v_crsd_my_stock_contacts_courses`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_crsd_my_stock_contacts_courses` AS (select `a`.`stock_id` AS `stock_id`,`a`.`c_date` AS `c_date`,`a`.`c_instock` AS `c_instock`,`a`.`c_distributed` AS `c_distributed`,`a`.`c_returned` AS `c_returned`,`a`.`user_id` AS `user_id`,`a`.`course_id` AS `course_id`,`a`.`receiver_id` AS `receiver_id`,`a`.`c_status` AS `c_status`,`b`.`first_name` AS `first_name`,`b`.`user_code` AS `user_code`,`c`.`course_title` AS `course_title`,`c`.`c_language_id` AS `c_language_id`,`c`.`course_code` AS `course_code`,`d`.`Language_in_English` AS `Language_in_English` from (((`eo_crsd_my_stock` `a` join `eo_cntd_contacts` `b`) join `eo_crsd_courses` `c`) join `eo_crsd_languages` `d`) where ((`a`.`course_id` = `c`.`course_id`) and (`a`.`user_id` = `b`.`user_id`) and (`c`.`c_language_id` = `d`.`Language_ID`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_languages_contacts`
--

/*!50001 DROP VIEW IF EXISTS `v_languages_contacts`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_languages_contacts` AS (select distinct `a`.`user_id` AS `user_id`,`a`.`contact_id` AS `contact_id`,`a`.`user_code` AS `user_code`,`a`.`first_name` AS `first_name`,`a`.`last_name` AS `last_name`,`a`.`ct_address1` AS `ct_address1`,`a`.`ct_address2` AS `ct_address2`,`a`.`ct_address3` AS `ct_address3`,`a`.`ct_address4` AS `ct_address4`,`a`.`ct_pincode` AS `ct_pincode`,`a`.`ct_mobile` AS `ct_mobile`,`a`.`ct_gender` AS `ct_gender`,`a`.`ct_mail_id` AS `ct_mail_id`,`a`.`ct_photo` AS `ct_photo`,`a`.`ct_assembly_reference` AS `ct_assembly_reference`,`a`.`ct_active` AS `ct_active`,`c`.`user_type_id` AS `user_type_id`,`c`.`user_type_description` AS `user_type_description`,`d`.`languages_assigned_id` AS `languages_assigned_id`,`d`.`language_id` AS `language_id`,`e`.`Language_in_English` AS `Language_in_English`,`e`.`Language_in_Local` AS `Language_in_Local` from ((((`eo_cntd_contacts` `a` join `eo_users` `b`) join `eo_user_types` `c`) join `eo_languages_assigned` `d`) join `eo_crsd_languages` `e`) where ((`a`.`user_id` = `b`.`user_id`) and (`b`.`user_type_id` = `c`.`user_type_id`) and (`d`.`language_id` = `e`.`Language_ID`) and (`a`.`user_id` = `d`.`user_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_languages_students`
--

/*!50001 DROP VIEW IF EXISTS `v_languages_students`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_languages_students` AS (select distinct `a`.`user_id` AS `user_id`,`a`.`student_id` AS `student_id`,`a`.`student_code` AS `student_code`,`a`.`first_name` AS `first_name`,`a`.`last_name` AS `last_name`,`a`.`st_address1` AS `st_address1`,`a`.`st_address2` AS `st_address2`,`a`.`st_address3` AS `st_address3`,`a`.`st_address4` AS `st_address4`,`a`.`st_pincode` AS `st_pincode`,`a`.`st_mobile` AS `st_mobile`,`a`.`st_gender` AS `st_gender`,`a`.`st_mail_id` AS `st_mail_id`,`a`.`st_photo` AS `st_photo`,`a`.`st_active` AS `st_active`,`c`.`user_type_id` AS `user_type_id`,`c`.`user_type_description` AS `user_type_description`,`d`.`languages_assigned_id` AS `languages_assigned_id`,`d`.`language_id` AS `language_id`,`e`.`Language_in_English` AS `Language_in_English`,`e`.`Language_in_Local` AS `Language_in_Local` from ((((`eo_stud_students` `a` join `eo_users` `b`) join `eo_user_types` `c`) join `eo_languages_assigned` `d`) join `eo_crsd_languages` `e`) where ((`a`.`user_id` = `b`.`user_id`) and (`b`.`user_type_id` = `c`.`user_type_id`) and (`d`.`language_id` = `e`.`Language_ID`) and (`a`.`user_id` = `d`.`user_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_languages_users`
--

/*!50001 DROP VIEW IF EXISTS `v_languages_users`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_languages_users` AS (select distinct `d`.`user_id` AS `user_id`,`d`.`languages_assigned_id` AS `languages_assigned_id`,`d`.`language_id` AS `language_id`,`e`.`Language_in_English` AS `Language_in_English`,`e`.`Language_in_Local` AS `Language_in_Local` from (`eo_languages_assigned` `d` join `eo_crsd_languages` `e`) where (`d`.`language_id` = `e`.`Language_ID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-17 19:23:13
